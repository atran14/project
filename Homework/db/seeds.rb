# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
insurances = Insurance.create ([
    {name: 'Farmer', street_address: '1234 Holy St'},
    {name: 'All State', street_address: '5647 Forest Rd'},
    {name: 'Geico', street_address: '3427 River Ave'},
    {name: 'America', street_address: '2389 Carrot Rd'},
    {name: 'Lone Star', street_address: '4578 Brocoli St'}

])

patients = Patient.create ([
    {name: 'Fred D', street_address: '1234 Red Wine St', insurance_id: 1},
    {name: 'Google Y', street_address: '2346 Onion Ave', insurance_id: 2},
    {name: 'Yahoo T', street_address: '4583 Little York', insurance_id: 3},
    {name: 'Perry H', street_address: '8377 Katy Freeway', insurance_id: 4},
    {name: 'Apple N', street_address: '4736 Jones Rd', insurance_id: 5}
])

specialists = Specialist.create([
    {name: 'Catherine Nguyen', specialty: 'Pediatric'},
    {name: 'Erika Le', specialty: 'Oncology'},
    {name: 'George Miller', specialty: 'Psychiatric'},
    {name: 'Kathy Berry', specialty: 'Allergy'},
    {name: 'Cherry Bully', specialty: 'Surgeon'}

     ])
appointments = Appointment.create([
    {complaint: 'Good', appointment_date: '2014-10-15',specialist_id: 1, patient_id: 3, fee: '50.0'},
    {complaint: 'Good', appointment_date: '2014-11-12',specialist_id: 2, patient_id: 4, fee: '199.99'},
    {complaint: 'Excellent', appointment_date: '2014-10-30',specialist_id: 5, patient_id: 5, fee: '0.99'},
    {complaint: 'Fine', appointment_date: '2014-10-20',specialist_id: 3, patient_id: 6, fee: '67.5'},
    {complaint: 'Not good at all', appointment_date: '2014-10-09',specialist_id: 4, patient_id: 7, fee: '29.76'},

                                  ])