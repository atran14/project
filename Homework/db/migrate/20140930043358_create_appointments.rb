class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.text :complaint
      t.date :appointment_date
      t.integer :specialist_id
      t.integer :patient_id
      t.belongs_to :patient_id
      t.belongs_to :specialist_id
      t.decimal :fee
      attr_accessible :fee
      t.timestamps
    end
  end
end
