class Appointment < ActiveRecord::Base
  belongs_to :patient
  belongs_to :specialist
  validates_presence_of :fee

end
