json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :complaint, :appointment_date, :specialist_id, :patient_id
  json.url appointment_url(appointment, format: :json)
end
